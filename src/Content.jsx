
//une seule exportation par défaut pour chaque fichier
//pour les autre content dans ce fichier on utilise export  function

export default function Content({prenom, nom, children}){ 
    /*let nom = props.nom;
    let nom = props.prenom;
    let children = props.children;
    */
    return (
        <>
            <h1>bonjour {prenom} {nom} de {children} </h1>
            <p> il est {new Date().toTimeString()}</p>

            {Youtube()}
        </>
    )
}

function Youtube() {
    return <iframe
        type='text/html'
        width={480}
        height={270}
        src='https://www.youtube.com/embed/M7lc1UVf-VE'
        title='Vidéo YouTube'
        frameBorder={0}
        allowFullscreen />
}

//ajouté comme exemple pour comprendre les export par défaut
export function Addition (a,b){
    return a+b
} 