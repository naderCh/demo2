
import { Fragment } from "react";
import {createRoot} from "react-dom/client"


let div = document.getElementById("root")
let root = createRoot(div)


//composant: commence par lettre MAJ + retourne du jsx
function Content(){ 
    return (
        // Fragment mieux que div: pas besoin d'ajouter un div dans le div parent 
        <Fragment>  
            <h1>bonjour à tous </h1>
            <p> il est {new Date().toTimeString()}</p>
        </Fragment>
    )
}

root.render(<Content />);

//maj tout les secondes
/*setInterval( () => {
                    root.render(<Content />);
                    },
             1000
             );
*/