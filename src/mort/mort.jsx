//react/vite
import { createElement } from "react"
import {createRoot} from "react-dom/client"



let div = document.getElementById("root")
let root = createRoot(div)
//composant de texte
root.render("<h1>bonjour</h1>")

//jsc mais en créant l'élement manuellement
let expr = createElement("h1",{title:'description'}, 'Bonjour à tous!')
root.render( expr )

//par objets DOM virtuel
let jsx = <h1 title= "description">Bonjour à tous!</h1>
root.render(jsx)

//or use vanilla JS
let div = document.getElementById("root")
div.innerHTML = "<h1>bonjour le monde!</h1>"

//par objets DOM
let element = { type:'h1',props:{ title: 'description'}, 
                children: 'boujour a tous!',
                key: null,
                ref: null,
                $$typeof: Symbol.for('react.element') //$$typeof: est nécessaire pour proterger contre injection contre 
 
                }
root.render(element)